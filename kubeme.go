package main

import (
	"fmt"
	"gitlab.com/kubeme/kubeconf"
	"os"
	"path/filepath"
	"runtime"

	"github.com/olekukonko/tablewriter"
	"github.com/spf13/pflag"
)

func main() {
	var kubepath *string = pflag.String("KUBECONFIG", "", "bitte geben sie einen Path an")
	pflag.Parse()
	fmt.Println(*kubepath)
	homeDir := getHomeDir()
	defaultKubeConfigPath := filepath.Join(homeDir, ".kube", "config")
	kubeConfigPaths := kubeconf.GetConfigPaths(defaultKubeConfigPath)
	kubeConfigs := kubeconf.GetKubeConfigs(kubeConfigPaths)
	printCombinedTable(kubeConfigs)
}

// Gibt das Home-Verzeichnis des Benutzers zurück.
func getHomeDir() string {
	if runtime.GOOS == "windows" {
		return os.Getenv("USERPROFILE")
	} else {
		return os.Getenv("HOME")
	}
}

// Gibt eine kombinierte Tabelle der Cluster- und Kontextinformationen aus.
func printCombinedTable(kubeConfigs map[string]kubeconf.KubeConfig) {
	table := tablewriter.NewWriter(os.Stdout)
	table.SetHeader([]string{"Datei", "Cluster Name", "Cluster Server", "Context Name", "User"})

	// Fülle die Tabelle mit Daten
	for path, kubeConfig := range kubeConfigs {
		addRow(kubeConfig, table, path)
	}

	table.Render()
}

func addRow(kubeConfig kubeconf.KubeConfig, table *tablewriter.Table, path string) {
	clusterServerMap := make(map[string]string)
	for _, cluster := range kubeConfig.Clusters {
		clusterServerMap[cluster.Name] = cluster.Cluster.Server
	}

	for _, context := range kubeConfig.Contexts {
		clusterServer := clusterServerMap[context.Context.Cluster]
		contextName := context.Name
		if contextName == kubeConfig.CurrentContext {
			contextName += " (current)"
		}
		table.Append([]string{path, context.Context.Cluster, clusterServer, contextName, context.Context.User})
	}
}
